mod lib;

pub fn config(arg: String, extension: String, filename: String) {
    let user = "$USER"; //Required by Elisp Org to pdf evaluation. Doom-emacs with latex and org instaled is required. Change according to your username.
    let compiler = match extension.as_ref() {
        "c" => format!("gcc {} -o {}", arg, filename),
        "cpp" => format!("g++ {} -o {}", arg, filename),
        "go" => format!("go build {}", arg),
        "hs" => format!("ghc {} -o {}", arg, filename),
        "nim" => format!("nim c {}", arg),
        "m" => format!("octave {}", arg),
        "ms" => format!(
            "preconv {} | refer -PS -e | groff -me -ms -kept -T pdf > {}.pdf",
            arg, filename
        ),
        "org" => format!(
            "emacs {} --batch -u {} -f org-latex-export-to-pdf",
            arg, user
        ),
        "py" => format!("python3 {}", arg),
        "rmd" | "Rmd" => format!("Rscript -e \"rmarkdown::render('{}', quiet=TRUE)\"", arg),
        "rs" => format!("rustc {}", arg),
        "tex" => format!("pdflatex {}", arg),
        _ => "echo Not Supported".to_string(),
    };
    lib::command(compiler);
}
