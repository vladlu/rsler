use std::process::Command;

pub fn command(compiler: String) {
    println!("{}", &compiler);
    let command = Command::new("sh")
        .arg("-c")
        .arg(&compiler)
        .output()
        .expect("Compiler Error");
    println!("{}", command.status);
}
