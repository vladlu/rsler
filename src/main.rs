use config::config;
mod config;
mod lib;
fn main() {
    let input = lib::arguments();
    let argc = lib::arg_count();
    let arg = &input[2];
    let extension = &input[1];
    let filename = &input[0];
    if argc == 2
        && (input[0] != *"exit" && input[1] != *"exit" && input[2] != "exit")
    {
        config(arg.to_string(), extension.to_string(), filename.to_string());
    } else if input[0] == *"exit"
        && input[1] == *"exit"
        && input[2] == *"exit"
    {
        eprintln!("USAGE:\n rsler [FILE] \n \n rsler: \n -h, --help : Get this menu");
    } else {
        eprintln!(
            "Wrong number of arguments supplied! \n
USAGE:\n rsler [FILE] \n \n rsler: \n -h, --help : Get this menu"
        )
    }
}
