use env::args;
use std::{convert::TryInto, env};

pub fn arguments() -> Vec<String> {
    let argv: Vec<String> = args().collect();
    let argc = arg_count();
    if argc == 2 {
        if argv[1] == *"-h" || argv[1] == *"--help" {
            return vec!["exit".to_string(), "exit".to_string(), "exit".to_string()];
        }
        let input: Vec<&str> = argv[1].rsplitn(2, '.').collect();
        let filename: String = input[1].to_string();
        let extension: String = input[0].to_string();
        let mut result = Vec::new();
        result.push(filename);
        result.push(extension);
        result.push(argv[1].clone());
        result
    } else {
        return vec![
            "blank".to_string(),
            "blank".to_string(),
            "blank".to_string(),
        ];
    }
}

pub fn arg_count() -> u8 {
    args().count().try_into().unwrap()
}
/*
struct Data {
    input: Vec<String>,
    arg: String,
    extension: String,
    filename: String,
}*/
